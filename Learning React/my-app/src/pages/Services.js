import React from 'react'
import Draggable from 'react-draggable';

export default function Services() {
    var { foo, bar } = { foo: "lorem", bar: "ipsum" };
    console.log(foo);
    return (
        <div style={{position:"relative", height:100}}>
        <Draggable bounds="parent">
        <div
          style={{ width: 200 , height:100}} className="draggable"
        >
        <h4 style={{ height: 20, userSelect: "none" }}>
              Drag Me
            </h4>
        </div>
      </Draggable>
      </div>
    )
}
