import React, { useState } from 'react'
import Container from '../components/Container'

export default function Home() {
    return (
        <div>
            <h1>Hello World</h1>
            <Container/>
        </div>
    )
}
