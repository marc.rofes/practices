import React, { useState } from 'react'

export default function FormTodo(props) {
    const [description, setDescription] = useState("")
    let handleChange = (event) => {
        setDescription(event.target.value);
    }
    const { handleAddItem } = props
    console.log(handleAddItem)
    let handleSubmit = (event) => {
        event.preventDefault();
        handleAddItem({
            done: false,
            id: (+new Date()).toString(),
            description
        });
        setDescription("");
    }
    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input type="text" value={description} onChange={handleChange} />
                <button disabled={description ? "" : "disabled"} onClick={handleSubmit}>Add</button>
            </form>
        </div>
    )
}
