import React from "react";
import "./Navbar.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "react-bootstrap/Navbar";
import Container from 'react-bootstrap/Container'
import { NavLink} from "react-router-dom";

export default function Navigation() {
  return (
    <>
    <Navbar bg="dark" variant="dark" expand="lg">
      <Container>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
      <Navbar.Brand>
        TTM
      </Navbar.Brand>
      <NavLink to="/" exact activeClassName="navbar__link--active" className="navbar__link">
          Home
        </NavLink>
        <NavLink to="/services" activeClassName="navbar__link--active" className="navbar__link">
          Services
        </NavLink>
        </Navbar.Collapse>
        </Container>
    </Navbar>
    </>
  );
}
