import React from 'react'

export default function Checkbox(props) {
    const { onChange, data: { id, description, done } } = props;
    return (
        <>
        <input
          className="todo__state"
          name={id}
          type="checkbox"
          defaultChecked={done}
          onChange={onChange}
        />
        <div className="todo__text">{description}</div>
        </>
    )
}
