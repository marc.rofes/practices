import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  withRouter
} from "react-router-dom";
import Navigation from "./components/Navbar";
import Home from "./pages/Home";
import Services from "./pages/Services";
import PageNotFound from "./pages/PageNotFound";

const Main = withRouter(({ location }) => {
  return(
    <div>
    {location.pathname !== "/404" && <Navigation/>}
    <Switch>
          <Route path="/services" component={Services} />
          <Route path="/" exact component={Home} />
          <Route path='/404' component={PageNotFound} />
          <Redirect to="/404" />
      </Switch>
    </div>
  )
})

const App = () => {
  return(<Router>
  <Main/>
  </Router>)
}
export default App;
